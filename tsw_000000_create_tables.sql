CREATE DATABASE TSW;
USE TSW;
/*TABLA PROVINCIA*/
CREATE TABLE provincia(
	id_provincia    INTEGER  NOT NULL PRIMARY KEY 
	,descripcion    VARCHAR(53) NOT NULL
	,latitud   	NUMERIC(17,13) NOT NULL
	,longitud   NUMERIC(17,13) NOT NULL
);
/*TABLA LOCALIDAD*/
CREATE TABLE localidad(
   id_localidad            BIGINT NOT NULL PRIMARY KEY
  ,id_provincia    		   INTEGER NOT NULL
  ,descripcion             VARCHAR(45) NOT NULL
  ,latitud                 NUMERIC(17,13) NOT NULL
  ,longitud                NUMERIC(17,13) NOT NULL
  ,CONSTRAINT FOREIGN KEY id_provincia (id_provincia) REFERENCES provincia (id_provincia) 
);
/*TABLA CLIENTE*/
CREATE TABLE cliente(
	 id_cliente    INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion    VARCHAR(50) NOT NULL
	,logo_cliente TEXT CHARACTER SET ascii
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA SUCURSAL*/
CREATE TABLE sucursal_cliente(
	 id_sucursal_cliente INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,id_cliente INTEGER NOT NULL
	,descripcion VARCHAR(50) NOT NULL
	,id_localidad BIGINT NOT NULL
	,direccion VARCHAR(200) NOT NULL
	,codigo_postal varchar(10) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
	,CONSTRAINT FOREIGN KEY id_cliente (id_cliente) REFERENCES cliente (id_cliente)
	,CONSTRAINT FOREIGN KEY id_localidad (id_localidad) REFERENCES localidad (id_localidad) 
);
/*TABLA CONTACTO*/
CREATE TABLE contacto_sucursal(
	 id_contacto_sucursal INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,id_sucursal_cliente INTEGER NOT NULL
	,nombre VARCHAR(100) NOT NULL
	,apellido VARCHAR(30) NOT NULL
	/*,mail VARCHAR(100) NULL
	,telefono INTEGER(10) NULL*/
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
	,CONSTRAINT FOREIGN KEY id_sucursal_cliente (id_sucursal_cliente) REFERENCES sucursal_cliente (id_sucursal_cliente) 
);
/*TABLA ESTADO PEDIDO*/
CREATE TABLE estado_pedido(
	 id_estado_pedido INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion VARCHAR(20) NOT NULL
	,observaciones VARCHAR(300) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA ESTADO STOCK*/
CREATE TABLE estado_stock(
	 id_estado_stock INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion VARCHAR(20) NOT NULL
	,observaciones VARCHAR(300) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA MODELO PRODUCTO*/
CREATE TABLE modelo_producto(
	 id_modelo_producto INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion VARCHAR(20) NOT NULL
	,observaciones VARCHAR(300) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA CONFIGURACION PRODUCTO*/
CREATE TABLE configuracion_producto(
	 id_configuracion_producto INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion VARCHAR(20) NOT NULL
	,observaciones VARCHAR(300) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA MARCA PRODUCTO*/
CREATE TABLE marca_producto(
	 id_marca_producto INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion VARCHAR(20) NOT NULL
	,observaciones VARCHAR(300) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA PROVEEDOR TRANSPORTE*/
CREATE TABLE proveedor_transporte(
	 id_proveedor_transporte INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,descripcion    VARCHAR(50) NOT NULL
	,logo_proveedor TEXT CHARACTER SET ascii
	,valor_maximo_permitido DECIMAL(11,2) NOT NULL DEFAULT 0
	,valor_por_remito DECIMAL(11,2) NOT NULL DEFAULT 0
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
);
/*TABLA PRODUCTO*/
CREATE TABLE producto(
	 id_producto INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,id_marca_producto INTEGER NOT NULL
	,id_modelo_producto INTEGER NOT NULL
	,descripcion VARCHAR(30) NOT NULL
	,observaciones VARCHAR(300) NULL
	,volumen_cm DECIMAL(11,2) NOT NULL
	,peso_real DECIMAL(11,2) NOT NULL
	,valor_unitario INTEGER NOT NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE 
	,CONSTRAINT FOREIGN KEY id_marca_producto (id_marca_producto) REFERENCES marca_producto(id_marca_producto)  	
	,CONSTRAINT FOREIGN KEY id_modelo_producto (id_modelo_producto) REFERENCES modelo_producto(id_modelo_producto) 
);
/*TABLA SLA_TRANSPORTE*/
CREATE TABLE sla_transporte(
	 id_sla_transporte BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT
	/*,compromiso_entrega INTEGER NOT NULL*/	
	,id_proveedor_transporte INTEGER NOT NULL
	/*,id_localidad BIGINT NOT NULL*/
	,codigo_postal varchar(10) NULL	
	,escala INTEGER NOT NULL /*esta escala es la misma de la tabla circuito_radial*/
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE 
	/*,CONSTRAINT FOREIGN KEY id_localidad (id_localidad) REFERENCES localidad (id_localidad)*/
	,CONSTRAINT FOREIGN KEY id_proveedor_transporte (id_proveedor_transporte) REFERENCES proveedor_transporte (id_proveedor_transporte) 
);

/*TABLA SLA*/
CREATE TABLE codigo_postal_sla(
	id_codigo_postal_sla BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT
	,codigo_postal INTEGER  NOT NULL
	,sla INTEGER(3) NOT NULL
	,ff_alta DATE NOT NULL
	,ff_baja DATE
);

/*TABLA CIRCUITO_RADIAL*/
CREATE TABLE circuito_radial(
	 id_circuito_radial INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,id_proveedor_transporte INTEGER NOT NULL
	,escala INTEGER NOT NULL
	,valor DECIMAL(6,2) NOT NULL
	,valor_adicional DECIMAL(6,2) NOT NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
	,CONSTRAINT FOREIGN KEY id_proveedor_transporte (id_proveedor_transporte) REFERENCES proveedor_transporte (id_proveedor_transporte) 
);
/*TABLA PEDIDO*/
CREATE TABLE pedido(
	 id_pedido BIGINT  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,nro_pedido BIGINT NOT NULL
	,id_sucursal_cliente INTEGER NULL
	,id_contacto_sucursal INTEGER NULL
	,telefono_contacto INTEGER(10) NULL
	,es_neutral BIT NOT NULL DEFAULT 0
	,nro_ticket BIGINT NULL
	,id_equipo VARCHAR(30) NULL
	,comentarios VARCHAR(300) NULL
	,id_estado_pedido INTEGER NOT NULL
	,observaciones_estado VARCHAR(300) NULL
	/*,id_producto INTEGER NOT NULL
	,id_marca_producto INTEGER NOT NULL
	,id_modelo_producto INTEGER NULL
	,id_configuracion_producto INTEGER NULL
	,cantidad INTEGER NOT NULL*/
	,ff_ingreso DATETIME NOT NULL
	,ff_sla DATE
	,ff_cumplimiento DATE
	,ff_envio_diferido DATE
	,ff_entrega DATE
	,ff_alta DATE NOT NULL
	,ff_baja DATE
	,id_proveedor_transporte INTEGER DEFAULT 0
	,CONSTRAINT FOREIGN KEY id_estado_pedido (id_estado_pedido) REFERENCES estado_pedido(id_estado_pedido)
	/*,CONSTRAINT FOREIGN KEY id_producto (id_producto) REFERENCES producto(id_producto)
	,CONSTRAINT FOREIGN KEY id_marca_producto (id_marca_producto) REFERENCES marca_producto(id_marca_producto)  	
	,CONSTRAINT FOREIGN KEY id_modelo_producto (id_modelo_producto) REFERENCES modelo_producto(id_modelo_producto) 
	,CONSTRAINT FOREIGN KEY id_configuracion_producto (id_configuracion_producto) REFERENCES configuracion_producto(id_configuracion_producto)*/
	,CONSTRAINT FOREIGN KEY id_proveedor_transporte (id_proveedor_transporte) REFERENCES proveedor_transporte(id_proveedor_transporte) ON DELETE SET NULL ON UPDATE CASCADE
);
/*TABLA PEDIDO_PRODUCTO*/
CREATE TABLE pedido_producto(
	 id_pedido_producto BIGINT  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,id_pedido BIGINT NOT NULL
	,id_producto INTEGER NOT NULL
	/*,id_marca_producto INTEGER NOT NULL
	,id_modelo_producto INTEGER NULL*/
	,id_configuracion_producto INTEGER NULL
	,cantidad INTEGER NOT NULL
	,cantidad_retorno INTEGER NOT NULL
	,ff_alta DATE NOT NULL
	,ff_baja DATE
	,CONSTRAINT FOREIGN KEY id_producto (id_producto) REFERENCES producto(id_producto)
	/*,CONSTRAINT FOREIGN KEY id_marca_producto (id_marca_producto) REFERENCES marca_producto(id_marca_producto)  	
	,CONSTRAINT FOREIGN KEY id_modelo_producto (id_modelo_producto) REFERENCES modelo_producto(id_modelo_producto)*/ 
	,CONSTRAINT FOREIGN KEY id_configuracion_producto (id_configuracion_producto) REFERENCES configuracion_producto(id_configuracion_producto)
);
/*TABLA STOCK*/
/*CREATE TABLE stock(
	 id_stock BIGINT  NOT NULL PRIMARY KEY AUTO_INCREMENT
	,id_producto INTEGER NOT NULL
	,id_modelo_producto INTEGER NULL
	,id_configuracion_producto INTEGER NULL
	,id_marca_producto INTEGER NOT NULL
	,codigo VARCHAR(30) NOT NULL
	,descripcion VARCHAR(100) NULL
	,id_estado_stock INTEGER NOT NULL
	,largo DECIMAL(6,2) NULL
	,ancho DECIMAL(6,2) NULL
	,alto DECIMAL(6,2) NULL
	,peso DECIMAL(6,2) NULL
	,valor DECIMAL(6,2) NULL
	,ff_alta   	DATE NOT NULL
	,ff_baja   	DATE
	,CONSTRAINT FOREIGN KEY id_producto (id_producto) REFERENCES producto (id_producto) 
	,CONSTRAINT FOREIGN KEY id_modelo_producto (id_modelo_producto) REFERENCES modelo_producto (id_modelo_producto) 
	,CONSTRAINT FOREIGN KEY id_configuracion_producto (id_configuracion_producto) REFERENCES configuracion_producto (id_configuracion_producto)
	,CONSTRAINT FOREIGN KEY id_marca_producto (id_marca_producto) REFERENCES marca_producto (id_marca_producto) 	
	,CONSTRAINT FOREIGN KEY id_estado_stock (id_estado_stock) REFERENCES estado_stock (id_estado_stock) 	
);*/
/*TABLA LOG_ERROR*/
CREATE TABLE log_error (
	id_log_error INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT,
    app	VARCHAR(50) NOT NULL,
	ff_log_error DATETIME NOT NULL,
	nivel VARCHAR(50) NOT NULL,
    mensaje TEXT DEFAULT NULL,
	logger VARCHAR(255) NOT NULL,
    callsite TEXT NOT NULL,
    excepcion TEXT NOT NULL
);
/*TABLA FERIADOS*/
CREATE TABLE feriado (
	id_feriado INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
    ,ff_feriado DATE NOT NULL
	,descripcion VARCHAR(100) NULL
	,ff_baja DATE NULL
);