USE TSW;
/*Nomenclatura: idx_field*/
/*TABLA PROVINCIA*/
CREATE INDEX idx_id_provincia ON provincia(id_provincia);

/*TABLA LOCALIDAD*/
CREATE INDEX idx_id_localidad ON localidad(id_localidad);
CREATE INDEX idx_id_provincia ON localidad(id_provincia);

/*TABLA CLIENTE*/
CREATE INDEX idx_id_cliente ON cliente(id_cliente);

/*TABLA SUCURSAL*/
CREATE INDEX idx_id_sucursal_cliente ON sucursal_cliente(id_sucursal_cliente);
CREATE INDEX idx_id_cliente ON sucursal_cliente(id_cliente);
CREATE INDEX idx_id_localidad ON sucursal_cliente(id_localidad);

/*TABLA CONTACTO*/
CREATE INDEX idx_id_contacto_sucursal ON contacto_sucursal(id_contacto_sucursal);
CREATE INDEX idx_id_sucursal_cliente ON contacto_sucursal(id_sucursal_cliente);

/*TABLA ESTADO PEDIDO*/
CREATE INDEX idx_id_estado_pedido ON estado_pedido(id_estado_pedido);

/*TABLA ESTADO STOCK*/
CREATE INDEX idx_id_estado_stock ON estado_stock(id_estado_stock);

/*TABLA MODELO PRODUCTO*/
CREATE INDEX idx_id_modelo_producto ON modelo_producto(id_modelo_producto);

/*TABLA CONFIGURACION PRODUCTO*/
CREATE INDEX idx_id_configuracion_producto ON configuracion_producto(id_configuracion_producto);

/*TABLA MARCA PRODUCTO*/
CREATE INDEX idx_id_marca_producto ON marca_producto(id_marca_producto);

/*TABLA PROVEEDOR TRANSPORTE*/
CREATE INDEX idx_id_proveedor_transporte ON proveedor_transporte(id_proveedor_transporte);

/*TABLA PRODUCTO*/
CREATE INDEX idx_id_producto ON producto(id_producto);
CREATE INDEX idx_id_marca_producto ON producto(id_marca_producto);
CREATE INDEX idx_id_modelo_producto ON producto(id_modelo_producto);

/*TABLA SLA_TRANSPORTE*/
CREATE INDEX idx_id_sla_transporte ON sla_transporte(id_sla_transporte);
CREATE INDEX idx_id_proveedor_transporte ON sla_transporte(id_proveedor_transporte);
/*CREATE INDEX idx_id_localidad ON sla_transporte(id_localidad);*/

/*TABLA PROVEEDOR TRANSPORTE*/
CREATE INDEX idx_id_codigo_postal_sla ON codigo_postal_sla(id_codigo_postal_sla);

/*TABLA CIRCUITO_RADIAL*/
CREATE INDEX idx_id_circuito_radial ON circuito_radial(id_circuito_radial);
CREATE INDEX idx_id_proveedor_transporte ON circuito_radial(id_proveedor_transporte);

/*TABLA PEDIDO*/
CREATE INDEX idx_id_pedido ON pedido(id_pedido);
CREATE INDEX idx_id_estado_pedido ON pedido(id_estado_pedido);
CREATE INDEX idx_id_proveedor_transporte ON pedido(id_proveedor_transporte);

/*TABLA PEDIDO_PRODUCTO*/
CREATE INDEX idx_id_pedido ON pedido_producto(id_pedido);
CREATE INDEX idx_id_producto ON pedido_producto(id_producto);
CREATE INDEX idx_id_configuracion_producto ON pedido_producto(id_configuracion_producto);

/*TABLA STOCK*/
/*CREATE INDEX idx_id_stock ON stock(id_stock);
CREATE INDEX idx_id_producto ON stock(id_producto);
CREATE INDEX idx_id_modelo_producto ON stock(id_modelo_producto);
CREATE INDEX idx_id_configuracion_producto ON stock(id_configuracion_producto);
CREATE INDEX idx_id_marca_producto ON stock(id_marca_producto);
CREATE INDEX idx_id_estado_stock ON stock(id_estado_stock);*/

/*LOG4NET*/
CREATE INDEX idx_id_log_error ON log_error(id_log_error);

/*TABLA FERIADOS*/
CREATE INDEX idx_id_feriado ON feriado(id_feriado);