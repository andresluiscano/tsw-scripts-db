USE TSW;
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-26.8753965086829,-54.6516966230371,54,'Misiones');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-33.7577257449137,-66.0281298195836,74,'San Luis');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-30.8653679979618,-68.8894908486844,70,'San Juan');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-32.0588735436448,-59.2014475514635,30,'Entre Ríos');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-48.8154851827063,-69.9557621671973,78,'Santa Cruz');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-40.4057957178801,-67.229329893694,62,'Río Negro');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-43.7886233529878,-68.5267593943345,26,'Chubut');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-32.142932663607,-63.8017532741662,14,'Córdoba');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-34.6298873058957,-68.5831228183798,50,'Mendoza');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-29.685776298315,-67.1817359694432,46,'La Rioja');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-27.3358332810217,-66.9476824299928,10,'Catamarca');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-37.1315537735949,-65.4466546606951,42,'La Pampa');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-27.7824116550944,-63.2523866568588,86,'Santiago del Estero');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-28.7743047046407,-57.8012191977913,18,'Corrientes');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-30.7069271588117,-60.9498369430241,82,'Santa Fe');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-26.9478001830786,-65.3647579441481,90,'Tucumán');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-38.6417575824599,-70.1185705180601,58,'Neuquén');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-24.2991344492002,-64.8144629600627,66,'Salta');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-26.3864309061226,-60.7658307438603,22,'Chaco');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-24.894972594871,-59.9324405800872,34,'Formosa');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-23.3200784211351,-65.7642522180337,38,'Jujuy');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-34.6144934119689,-58.4458563545429,02,'Ciudad Autónoma de Buenos Aires');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-36.6769415180527,-60.5588319815719,06,'Buenos Aires');
INSERT INTO provincia(latitud,longitud,id_provincia,descripcion) VALUES (-82.52151781221,-50.7427486049785,94,'Tierra del Fuego, Antártida e Islas del Atlántico Sur');